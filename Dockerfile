FROM maven:3.6.3-jdk-11
COPY ./eureka .
RUN mvn clean package

FROM openjdk:11
COPY --from=0 /target/*.jar .
CMD java -jar *.jar
