# Documentation of the Discovery Service

## Purpose

The Discovery service enables communication between services within the micro service architecture. Thanks to the discovery service, neither hostname nor port need to be known for communication.



## Usage

### Registering

Services that want to register with the Discovery Service must add the Eureka client dependency and annotate the `SpringBootApplication` class with `EnableEurekaClient`. Services must also add the following configuration to the `application.properties` file:

```
eureka.client.register-with-eureka = true
eureka.client.fetch-registry = true
eureka.client.serviceUrl.defaultZone = http://service-discovery:8761/eureka
```



### Communicating

There are two ways to communicate with a registered service.

1. Use the `DiscoveryClient` class and `getApplication` to identify the hostname and port of the service using the application name of the service specified in the `application.properties` file.
2. Use `RestTemplate` or `WebClient` annotated with `LoadBalanced` and use the application name of the service specified in the `application.properties` file as host name for URIs.

