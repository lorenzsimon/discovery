package de.unipassau.ep.boogle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * The class provides the main method and is used to configure the application.
 */
@SpringBootApplication
@EnableEurekaServer
public class BoogleApplication {

	/**
	 * Represent the start point of the application.
	 *
	 * @param args Arguments that are passed at the start.
	 */
	public static void main(String[] args) {
		SpringApplication.run(BoogleApplication.class, args);
	}

}
